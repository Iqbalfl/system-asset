<form class="delete" action="{{ $form_url ?? '' }}" method="post" id="delete-form">
    {{csrf_field()}}
    {{method_field('delete')}}
    @isset ($show_url)
        <a href="{{ $show_url }}" class="btn btn-circle btn-sm btn-info" title="Lihat Detail"><i class="fa fa-fw fa-eye"></i></a>
    @endisset
    @isset ($edit_url)
        @isset($cond)
            @if ($model->status != 200)
                <a href="{{ $edit_url }}" class="btn btn-circle btn-sm btn-warning" title="Edit Data"><i class="fa fa-fw fa-edit"></i></a>
            @endif
        @else
            <a href="{{ $edit_url }}" class="btn btn-circle btn-sm btn-warning" title="Edit Data"><i class="fa fa-fw fa-edit"></i></a>
        @endisset
    @endisset
    @isset ($form_url)
        @isset($cond)
            @if ($model->status != 200)
                <button type="submit" value="Delete" class="btn btn-circle btn-sm btn-danger js-submit-confirm" title="Hapus Data">
                    <i class="fa fa-trash"></i>
                </button>
            @endif
        @else
            <button type="submit" value="Delete" class="btn btn-circle btn-sm btn-danger js-submit-confirm" title="Hapus Data">
                <i class="fa fa-trash"></i>
            </button>
        @endisset
    @endisset
    @isset ($verif_url)
        @if ($model->status == 100 && Auth::user()->role == 'admin')
            <a href="{{ $verif_url }}" class="btn btn-circle btn-sm btn-success" title="Konfimrasi Pengajuan"><i class="fa fa-fw fa-check"></i></a>
        @endif
    @endisset
    @isset ($return_url)
        @if ($model->status == 200 && Auth::user()->id == $model->user_id)
            <a href="javascript:void(0)" class="btn btn-circle btn-sm btn-secondary" title="Kembalikan" onclick="showReturnModal({{ $model->id }})"><i class="fa fa-fw fa-redo"></i></a>
        @endif
    @endisset
</form>