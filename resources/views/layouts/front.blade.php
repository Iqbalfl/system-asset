<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>{{ config('app.name') }}</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{ asset('assets/stisla/modules/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/stisla/modules/fontawesome/css/all.min.css') }}">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="{{ asset('assets/stisla/modules/izitoast/css/iziToast.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/stisla/modules/datatables/datatables.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/stisla/modules/select2/dist/css/select2.min.css') }}">
  @yield('css')

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ asset('assets/stisla/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/stisla/css/custom.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/stisla/css/components.css') }}">
</head>

<body class="layout-3">
  <div id="app">
    <div class="main-wrapper container">
      <div class="navbar-bg bg-white shadow-sm" style="max-height: 65px;"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        {{-- <a href="{{ url('/') }}" class="navbar-brand sidebar-gone-hide">{{ config('app.name') }}</a> --}}
        <img alt="image" src="{{ asset('assets/stisla/img/sf-logo.png') }}" style="width: 150px;">
        <div class="navbar-nav">
          <a href="#" class="nav-link sidebar-gone-show" data-toggle="sidebar"><i class="fas fa-bars"></i></a>
        </div>
        <div class="nav-collapse">
          <ul class="navbar-nav ml-4">
            <li class="nav-item">
              <a class="nav-link text-dark font-weight-bold" href="{{ url('/') }}">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-dark font-weight-bold" href="{{ route('page.about') }}">Tentang</a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-dark font-weight-bold" href="{{ route('page.service') }}">Layanan</a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-dark font-weight-bold" href="{{ route('page.account') }}">Daftar Rekening</a>
            </li>
          </ul>
        </div>
        {{-- <form class="form-inline" action="{{ route('landing') }}">
          <ul class="navbar-nav">
            <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li>
          </ul>
          <div class="search-element">
            <input class="form-control" type="search" name="q" value="{{ $q ?? '' }}" placeholder="Cari Karyawan" aria-label="Search" data-width="500">
            <button class="btn" type="submit"><i class="fas fa-search"></i></button>
          </div>
        </form> --}}
        <ul class="navbar-nav navbar-right ml-auto">
          @auth
            <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
              @if (is_null(Auth::user()->avatar))
              <img alt="image" src="{{ asset('assets/stisla/img/avatar/avatar-1.png') }}" class="rounded-circle mr-1">
              @else
              <img alt="image" src="{{asset('uploads/images/avatars/'.Auth::user()->avatar)}}" class="rounded-circle mr-1">
              @endif
              <div class="d-sm-none d-lg-inline-block">Hi, {{ Auth::user()->name }}</div></a>
              <div class="dropdown-menu dropdown-menu-right">
                <a href="{{ route('profile.show') }}" class="dropdown-item has-icon">
                  <i class="far fa-user"></i> Profile
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item has-icon text-primary" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
                    <i class="fas fa-sign-out-alt"></i> {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
              </div>
            </li>
          @endauth
          @guest
            <li class="nav-item mr-2"><a href="{{ route('login') }}" class="btn btn-danger">Login</a></li>
            <li class="nav-item"><a href="{{ route('register') }}" class="btn btn-danger">Register</a></li>
          @endguest
        </ul>
      </nav>

      <!-- Main Content -->
      <div class="main-content" style="padding-top: 80px;">
        @yield('content')
      </div>
      {{-- <footer class="main-footer">
        <div class="footer-left">
          Copyright &copy; {{ date('Y') }} <div class="bullet"></div> {{ config('app.name') }}
        </div>
        <div class="footer-right">
          Bandung - Indonesia
        </div>
      </footer> --}}
    </div>
    @yield('content-slide')
  </div>

  <!-- General JS Scripts -->
  <script src="{{ asset('assets/stisla/modules/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/stisla/modules/popper.js') }}"></script>
  <script src="{{ asset('assets/stisla/modules/tooltip.js') }}"></script>
  <script src="{{ asset('assets/stisla/modules/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('assets/stisla/modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
  <script src="{{ asset('assets/stisla/modules/moment.min.js') }}"></script>
  <script src="{{ asset('assets/stisla/modules/chart.min.js') }}"></script>
  <script src="{{ asset('assets/stisla/js/stisla.js') }}"></script>
  
  <!-- JS Libraies -->
  <script src="{{ asset('assets/stisla/modules/izitoast/js/iziToast.min.js') }}"></script>
  <script src="{{ asset('assets/stisla/modules/datatables/datatables.min.js') }}"></script>
  <script src="{{ asset('assets/stisla/modules/jscolor.js') }}"></script>
  <script src="{{ asset('assets/stisla/modules/bs-custom-file-input.min.js') }}"></script>
  <script src="{{ asset('assets/stisla/modules/sweetalert/sweetalert.min.js') }}"></script>
  <script src="{{ asset('assets/stisla/modules/select2/dist/js/select2.js') }}"></script>

  <!-- Template JS File -->
  <script src="{{ asset('assets/stisla/js/scripts.js') }}"></script>
  <script src="{{ asset('assets/stisla/js/custom.js') }}"></script>
  @include('partials._toast')
  @yield('script')
</body>
</html>