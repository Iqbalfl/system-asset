@extends('layouts.main')

@section('content')
  <section class="section">
    <div class="section-header">
      <h1>Laporan Aset</h1>
    </div>

    <div class="row">
      <div class="col-lg-12">

        <!-- Card Peminjaman Asset -->
        <div class="card card-primary">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">Daftar Peminjaman Aset</h6>
          </div>
          <div class="card-body">

          <div class="table-responsive">
            <table class="table table-striped datatable_borrow">
              <thead>                                 
                <tr>
                  <th>#</th>
                  <th>Nama</th>
                  <th>Jenis</th>
                  <th>Tahun Anggaran</th>
                  <th>Harga</th>
                  <th>Dipinjam Oleh</th>
                  <th>Tanggal Peminjaman</th>
                  <th>Tanggal Pengembalian</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>

          </div>
        </div>
      </div>

      <div class="col-lg-12">

        <!-- Card Pengajuan Aset -->
        <div class="card card-primary">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">Daftar Pengajuan Aset</h6>
          </div>
          <div class="card-body">

          <div class="table-responsive">
            <table class="table table-striped datatable_submission">
              <thead>                                 
                <tr>
                  <th>#</th>
                  <th>Nama</th>
                  <th>Jenis</th>
                  <th>Tahun Anggaran</th>
                  <th>Harga</th>
                  <th>Keterangan</th>
                  <th>Diajukan Oleh</th>
                  <th>Tanggal Pengajuan</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>

          </div>
        </div>
      </div>
    </div>
  </section>

  <form id="form-return" action="{{ route('borrow.return.store') }}" method="POST">
    @csrf
    @method('PATCH')
    <input name="borrow_id" id="borrow_id" type="hidden" value="">
  </form>
@endsection
@section('script')
<script>
  $(document).ready(function() {
      $('.datatable_borrow').DataTable({
          processing: true,
          serverSide: true,
          autoWidth: false,
          language: {
              url: '{{ asset('assets/stisla/modules/datatables/lang/Indonesian.json') }}'
          },
          ajax: {
            url: '{{ route('borrow.index') }}',
            data: function (d) {
              d.status = $('select[name=status]').val()
            }
          },
          columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'asset.name', name: 'asset.name'},
            {data: 'asset.type', name: 'asset.type'},
            {data: 'asset.year', name: 'asset.year'},
            {data: null, name: 'asset.price', render: function ( data, type, row ) {
              return 'Rp ' + numberFormat(parseInt(data.asset.price));
            }},
            {data: 'user.name', name: 'user.name'},
            {data: 'display_borrow_date', name: 'borrow_date'},
            {data: 'display_return_date', name: 'return_date'},
            {data: 'display_status', name: 'status'},
          ]
      });

      $('.datatable_submission').DataTable({
          processing: true,
          serverSide: true,
          autoWidth: false,
          language: {
              url: '{{ asset('assets/stisla/modules/datatables/lang/Indonesian.json') }}'
          },
          ajax: {
            url: '{{ route('submission.index') }}',
            data: function (d) {
              d.status = $('select[name=status]').val()
            }
          },
          columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'name', name: 'name'},
            {data: 'type', name: 'type'},
            {data: 'year', name: 'year'},
            {data: null, name: 'price', render: function ( data, type, row ) {
              return 'Rp ' + numberFormat(parseInt(data.price));
            }},
            {data: 'description', name: 'description'},
            {data: 'user.name', name: 'user.name'},
            {data: 'display_submission_date', name: 'submission_date'},
            {data: 'display_status', name: 'status'},
          ]
      });

      $('#btn-filter').click(function(){
         $('.datatable').DataTable().draw(true);
      });

      $(document).on('click','.js-submit-confirm', function(e){
          e.preventDefault();
          swal({
            title: 'Apakah anda yakin ingin menghapus?',
            text: 'Data yang sudah dihapus, tidak dapat dikembalikan!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              $(this).closest('form').submit();
            } 
          });
      });
  });

  function numberFormat(x) {
    return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
  }

  const capitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
  }
</script>
@endsection
