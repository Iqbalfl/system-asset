@extends('layouts.main')

@section('content')
  <section class="section">
    <div class="section-header">
      <h1>Detail Pengajuan Aset</h1>
    </div>

    <form method="POST" action="{{ route('submission.update', $submission->id) }}" enctype="multipart/form-data">
      <div class="row">
        <div class="col-lg-12">
          <div class="card card-primary">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold">Informasi Pengajuan Aset</h6>
            </div>
            <div class="card-body">
              {{ csrf_field() }}
              @method('PUT')

              <div class="row">
                <div class="form-group col-6 {{ $errors->has('name') ? ' has-error' : '' }}">
                  <label for="name">Nama Aset</label>
                  <input id="name" type="text" class="form-control @if ($errors->has('name')) is-invalid @endif" name="name" tabindex="1" value="{{ $submission->name }}" disabled>
                  @if ($errors->has('name'))
                    <div class="invalid-feedback">
                      {{ $errors->first('name') }}
                    </div>
                  @endif
                </div>
                
                <div class="form-group col-6 {{ $errors->has('type') ? ' has-error' : '' }}">
                    <label for="type">Jenis Aset</label>
                    <select name="type" class="form-control select2 @if ($errors->has('type')) is-invalid @endif" data-placeholder="Pilih Jenis Aset" disabled>
                      <option value=""></option>
                      @foreach ($types as $item)
                        <option value="{{ $item }}" @if ($submission->type == $item) selected @endif>{{ $item }}</option>
                      @endforeach
                    </select>
                    @if ($errors->has('type'))
                      <div class="invalid-feedback">
                        {{ $errors->first('type') }}
                      </div>
                    @endif
                  </div>
              </div>

              <div class="row">
                <div class="form-group col-6 {{ $errors->has('year') ? ' has-error' : '' }}">
                  <label for="year">Tahun Anggaran</label>
                  <input id="year" type="number" min="1900" max="2099" class="form-control @if ($errors->has('year')) is-invalid @endif" name="year" tabindex="1" value="{{ $submission->year }}" disabled>
                  @if ($errors->has('year'))
                    <div class="invalid-feedback">
                      {{ $errors->first('year') }}
                    </div>
                  @endif
                </div>

                <div class="form-group col-6 {{ $errors->has('price') ? ' has-error' : '' }}">
                  <label for="price">Harga Aset</label>
                  <input id="price" type="text" class="form-control @if ($errors->has('price')) is-invalid @endif" name="price" tabindex="1" value="{{ rupiah($submission->price) }}" disabled>
                  @if ($errors->has('price'))
                    <div class="invalid-feedback">
                      {{ $errors->first('price') }}
                    </div>
                  @endif
                </div>
              </div>

              <div class="row">
                <div class="form-group col-6 {{ $errors->has('purchase_date') ? ' has-error' : '' }}">
                  <label for="purchase_date">Tanggal Beli Aset</label>
                  <input id="purchase_date" type="date" class="form-control @if ($errors->has('purchase_date')) is-invalid @endif" name="purchase_date" tabindex="1" value="{{ $submission->purchase_date }}" disabled>
                  @if ($errors->has('purchase_date'))
                    <div class="invalid-feedback">
                      {{ $errors->first('purchase_date') }}
                    </div>
                  @endif
                </div>

                <div class="form-group col-6 {{ $errors->has('expire_date') ? ' has-error' : '' }}">
                  <label for="expire_date">Tanggal Layak Aset</label>
                  <input id="expire_date" type="date" class="form-control @if ($errors->has('expire_date')) is-invalid @endif" name="expire_date" tabindex="1" value="{{ $submission->expire_date }}" disabled>
                  @if ($errors->has('expire_date'))
                    <div class="invalid-feedback">
                      {{ $errors->first('expire_date') }}
                    </div>
                  @endif
                </div>
              </div>

              <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                <label for="description">Keterangan</label>
                <textarea name="description" id="description" class="form-control @if ($errors->has('description')) is-invalid @endif" disabled>{{ $submission->description }}</textarea>
                @if ($errors->has('description'))
                  <div class="invalid-feedback">
                    {{ $errors->first('description') }}
                  </div>
                @endif
              </div>

              <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
                <label for="status">Status Aset</label>
                <input id="status" type="text" class="form-control @if ($errors->has('status')) is-invalid @endif" name="status" tabindex="1" value="{{ $submission->display_status }}" disabled>
                @if ($errors->has('status'))
                  <div class="invalid-feedback">
                    {{ $errors->first('status') }}
                  </div>
                @endif
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </form>
  </section>
@endsection

@section('script')
  <script>
    $(document).ready(function () {
      bsCustomFileInput.init()
      $('.select2').select2();
    })
  </script>
@endsection