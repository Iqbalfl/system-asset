@extends('layouts.main')

@section('content')
  <section class="section">
    <div class="section-header">
      <h1>Peminjaman Aset</h1>
    </div>

    <div class="d-sm-flex align-items-center justify-content-start mb-4">
        <a class="btn btn-sm btn-primary" href="{{ route('borrow.create') }}"><i class="fa fa-plus"></i> Ajukan Peminjaman</a> 
      <div class="form-inline ml-auto">
        <label>Filter Status</label>
        <select name="status" class="form-control-sm">
          <option value="all">Semua</option>
          <option value="100">Menunggu Pesetujuan</option>
          <option value="200">Disetujui</option>
          <option value="10">Ditolak</option>
        </select>
        <button class="btn btn-sm btn-primary ml-2" id="btn-filter"><i class="fas fa-filter"></i></button>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">

        <!-- Basic Card Example -->
        <div class="card card-primary">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">List Peminjaman Aset</h6>
          </div>
          <div class="card-body">

          <div class="table-responsive">
            <table class="table table-striped datatable">
              <thead>                                 
                <tr>
                  <th>#</th>
                  <th>Nama</th>
                  <th>Jenis</th>
                  <th>Tahun Anggaran</th>
                  <th>Harga</th>
                  <th>Dipinjam Oleh</th>
                  <th>Tanggal Peminjaman</th>
                  <th>Tanggal Pengembalian</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>

          </div>
        </div>
      </div>
    </div>
  </section>

  <form id="form-return" action="{{ route('borrow.return.store') }}" method="POST">
    @csrf
    @method('PATCH')
    <input name="borrow_id" id="borrow_id" type="hidden" value="">
  </form>
@endsection
@section('script')
<script>
  $(document).ready(function() {
      $('.datatable').DataTable({
          processing: true,
          serverSide: true,
          autoWidth: false,
          language: {
              url: '{{ asset('assets/stisla/modules/datatables/lang/Indonesian.json') }}'
          },
          ajax: {
            url: '{{ route('borrow.index') }}',
            data: function (d) {
              d.status = $('select[name=status]').val()
            }
          },
          columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'asset.name', name: 'asset.name'},
            {data: 'asset.type', name: 'asset.type'},
            {data: 'asset.year', name: 'asset.year'},
            {data: null, name: 'asset.price', render: function ( data, type, row ) {
              return 'Rp ' + numberFormat(parseInt(data.asset.price));
            }},
            {data: 'user.name', name: 'user.name'},
            {data: 'display_borrow_date', name: 'borrow_date'},
            {data: 'display_return_date', name: 'return_date'},
            {data: 'display_status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
          ]
      });

      $('#btn-filter').click(function(){
         $('.datatable').DataTable().draw(true);
      });

      $(document).on('click','.js-submit-confirm', function(e){
          e.preventDefault();
          swal({
            title: 'Apakah anda yakin ingin menghapus?',
            text: 'Data yang sudah dihapus, tidak dapat dikembalikan!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              $(this).closest('form').submit();
            } 
          });
      });
  });

  function numberFormat(x) {
    return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
  }

  const capitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
  }

  function showReturnModal(id) {
    $('#borrow_id').val(id);
    swal({
      title: 'Apakah anda yakin?',
      text: 'Akan mengembalikan aset ini',
      icon: 'warning',
      buttons: true,
      dangerMode: true,
    })
    .then((willReturn) => {
      if (willReturn) {
        $('#form-return').submit();
      } 
    });
  }
</script>
@endsection
