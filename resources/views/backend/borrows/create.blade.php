@extends('layouts.main')

@section('content')
  <section class="section">
    <div class="section-header">
      <h1>Peminjaman Pengajuan Aset</h1>
    </div>

    <form method="POST" action="{{ route('borrow.store') }}" enctype="multipart/form-data">
      <div class="row">
        <div class="col-lg-12">
          <div class="card card-primary">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold">Form Peminjaman Aset</h6>
            </div>
            <div class="card-body">
              {{ csrf_field() }}

              <div class="row">                
                <div class="form-group col-12 {{ $errors->has('asset_id') ? ' has-error' : '' }}">
                    <label for="asset_id">Pilih Aset</label>
                    <select name="asset_id" class="form-control select2 @if ($errors->has('asset_id')) is-invalid @endif" data-placeholder="Pilih Aset">
                      <option value=""></option>
                      @foreach ($assets as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                      @endforeach
                    </select>
                    @if ($errors->has('asset_id'))
                      <div class="invalid-feedback">
                        {{ $errors->first('asset_id') }}
                      </div>
                    @endif
                  </div>
              </div>

              <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block" tabindex="4">
                  Simpan
                </button>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </form>
  </section>
@endsection

@section('script')
  <script>
    $(document).ready(function () {
      bsCustomFileInput.init()
      $('.select2').select2();
    })
  </script>
@endsection