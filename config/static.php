<?php

return [
    'type' => [
        'atk' => 'Alat Tulis Kantor',
        'elektronik' => 'Peralatan Elektronik',
        'toolkit' => 'Toolkit',
        'mebel' => 'Mebel',
        'lainnya' => 'Lainnya',
    ],
];
