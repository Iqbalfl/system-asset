<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_submissions', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->string('name');
            $table->string('type');
            $table->integer('year');
            $table->date('submission_date');
            $table->date('purchase_date');
            $table->date('expire_date');
            $table->string('source')->nullable();
            $table->unsignedInteger('price')->default(0);
            $table->text('description')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->integer('status')->default(100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_submissions');
    }
}
