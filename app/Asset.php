<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    protected $appends = ['display_status', 'display_purchase_date', 'display_expire_date'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    
    public function borrow()
    {
        return $this->hasOne('App\Borrow', 'asset_id')->whereNull('return_date');
    }

    public function getDisplayPurchaseDateAttribute()
    {
        $date = $this->purchase_date;
        return date_dmy($date);
    }

    public function getDisplayExpireDateAttribute()
    {
        $date = $this->expire_date;
        return date_dmy($date);
    }

    public function getDisplayStatusAttribute()
    {
        $result = '';
        $status = $this->status;

        switch ($status) {
            case '100':
                $result = 'Tersedia';
                break;
            case '200':
                $result = 'Dipinjam';
                break;
            
            default:
                $result = 'Tidak Ada';
                break;
        }

        return $result;
    }
}
