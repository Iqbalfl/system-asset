<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $appends = ['display_status'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function ticket()
    {
        return $this->belongsTo('App\Ticket', 'ticket_id');
    }

    public function payments()
    {
        return $this->hasMany('App\Payment', 'order_id');
    }

    public function getDisplayStatusAttribute()
    {
        $result = '';
        $status = $this->status;

        switch ($status) {
            case '100':
                $result = 'Menunggu Pembayaran';
                break;
            case '150':
                $result = 'Menunggu Verifikasi Pembayaran';
                break;
            case '200':
                $result = 'Selesai';
                break;
            case '10':
                $result = 'Dibatalkan';
                break;
            
            default:
                $result = 'Tidak Ada';
                break;
        }

        return $result;
    }
}
