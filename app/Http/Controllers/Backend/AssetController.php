<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Session;
use App\Asset;
use App\AssetSubmission;


class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $assets = Asset::with('user');

            if ($request->get('status') != null) {
                if ($request->status != 'all') {
                    $assets->whereStatus($request->status);
                }
            }

            $assets = $assets->select('assets.*');

            return Datatables::of($assets)
                ->addIndexColumn()
                ->addColumn('action', function ($asset) {
                    return view('partials._action', [
                        'model'           => $asset,
                        // 'form_url'        => route('asset.destroy', $asset->id),
                        // 'edit_url'        => route('asset.edit', $asset->id),
                        'show_url'        => route('asset.show', $asset->id),
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('backend.assets.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $asset = Asset::find($id);
        $types = config('static.type');

        return view('backend.assets.show')->with(compact('types', 'asset'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
