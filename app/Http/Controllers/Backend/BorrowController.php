<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Session;
use App\Borrow;
use App\Asset;
use App\AssetSubmission;
use Illuminate\Support\Facades\Auth;

class BorrowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $user = Auth::user();

            $borrows = Borrow::with('user', 'asset');

            if ($request->get('status') != null) {
                if ($request->status != 'all') {
                    $borrows->whereStatus($request->status);
                }
            }

            if ($user->role == 'user') {
                $borrows->where('user_id', $user->id);
            }

            $borrows = $borrows->select('borrows.*');

            return Datatables::of($borrows)
                ->addIndexColumn()
                ->addColumn('action', function ($borrow) {
                    return view('partials._action', [
                        'model'           => $borrow,
                        // 'form_url'        => route('borrow.destroy', $borrow->id),
                        // 'edit_url'        => route('borrow.edit', $borrow->id),
                        'show_url'        => route('borrow.show', $borrow->id),
                        'verif_url'       => route('borrow.verification', $borrow->id),
                        'return_url'      => true,
                        'cond'            => true,
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('backend.borrows.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $assets = Asset::where('status', 100)->get();

        return view('backend.borrows.create')->with(compact('assets'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'asset_id' => 'required|numeric',
        ]);

        $asset_id = $request->asset_id;
        $user_id = Auth::user()->id;

        $model = new Borrow();
        $model->asset_id = $asset_id;
        $model->user_id = $user_id;
        $model->borrow_date = date('Y-m-d');
        $model->status = 100;
        $model->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Aset $model->name berhasil diajukan"
        ]);

        return redirect()->route('borrow.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $borrow = Borrow::with('user', 'asset')->find($id);
        $types = config('static.type');

        return view('backend.borrows.show')->with(compact('borrow', 'types'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function verification($id)
    {
        $borrow = Borrow::with('user', 'asset')->find($id);
        $types = config('static.type');

        return view('backend.borrows.verification')->with(compact('types', 'borrow'));
    }

    public function verificationStore(Request $request, $id)
    {
        $this->validate($request, [
            'status' => 'required|numeric',
        ]);

        $status = $request->status;

        $model = Borrow::find($id);
        $model->status = $status;
        $model->save();

        if ($status == 200) {
            $asset = Asset::find($model->asset_id);
            $asset->status = 200;
            $asset->save();
        }

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Peminjaman aset berhasil diverifikasi"
        ]);

        return redirect()->route('borrow.index');
    }

    public function returnStore(Request $request)
    {
        $this->validate($request, [
            'borrow_id' => 'required|numeric',
        ]);

        $borrow_id = $request->borrow_id;

        $model = Borrow::find($borrow_id);
        $model->status = 300;
        $model->return_date = date('Y-m-d');
        $model->save();

        $asset = Asset::find($model->asset_id);
        $asset->status = 100;
        $asset->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Aset $asset->name berhasil dikembalikan"
        ]);

        return redirect()->route('borrow.index');
    }
}
