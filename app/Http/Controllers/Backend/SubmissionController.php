<?php

namespace App\Http\Controllers\Backend;

use App\Asset;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Session;
use App\AssetSubmission;
use Illuminate\Support\Facades\Auth;

class SubmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $user = Auth::user();

            $submissions = AssetSubmission::with('user');

            if ($request->get('status') != null) {
                if ($request->status != 'all') {
                    $submissions->whereStatus($request->status);
                }
            }

            if ($user->role == 'user') {
                $submissions->where('user_id', $user->id);
            }

            $submissions = $submissions->select('asset_submissions.*');

            return Datatables::of($submissions)
                ->addIndexColumn()
                ->addColumn('action', function ($submission) {
                    return view('partials._action', [
                        'model'           => $submission,
                        'form_url'        => route('submission.destroy', $submission->id),
                        'edit_url'        => route('submission.edit', $submission->id),
                        'show_url'        => route('submission.show', $submission->id),
                        'verif_url'       => route('submission.verification', $submission->id),
                        'cond'            => true,
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('backend.submissions.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = config('static.type');

        return view('backend.submissions.create')->with(compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'type' => 'required|string|max:255',
            'year' => 'required|numeric',
            'price' => 'required|numeric',
            'purchase_date' => 'required|date',
            'expire_date' => 'required|date',
            'description' => 'required|string',
        ]);

        $model = new AssetSubmission();
        $model->name = $request->name;
        $model->user_id = auth()->user()->id;
        $model->type = $request->type;
        $model->year = $request->year;
        $model->price = $request->price;
        $model->submission_date = date('Y-m-d');
        $model->purchase_date = $request->purchase_date;
        $model->expire_date = $request->expire_date;
        $model->description = $request->description;
        $model->status = 100;

        $model->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Aset $model->name berhasil dibuat"
        ]);

        return redirect()->route('submission.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $submission = AssetSubmission::find($id);
        $types = config('static.type');

        return view('backend.submissions.show')->with(compact('types', 'submission'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $submission = AssetSubmission::find($id);
        $types = config('static.type');

        return view('backend.submissions.edit')->with(compact('types', 'submission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'type' => 'required|string|max:255',
            'year' => 'required|numeric',
            'price' => 'required|numeric',
            'purchase_date' => 'required|date',
            'expire_date' => 'required|date',
            'description' => 'required|string',
        ]);

        $model = AssetSubmission::find($id);
        $model->name = $request->name;
        $model->type = $request->type;
        $model->year = $request->year;
        $model->price = $request->price;
        $model->purchase_date = $request->purchase_date;
        $model->expire_date = $request->expire_date;
        $model->description = $request->description;
        $model->status = 100;

        $model->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Aset $model->name berhasil dibuat"
        ]);

        return redirect()->route('submission.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = AssetSubmission::find($id);
        $model->delete();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Berhasil menghapus data"
        ]);

        return redirect()->route('submission.index');
    }

    public function verification($id)
    {
        $submission = AssetSubmission::find($id);
        $types = config('static.type');

        return view('backend.submissions.verification')->with(compact('types', 'submission'));
    }

    public function verificationStore(Request $request, $id)
    {
        $this->validate($request, [
            'status' => 'required|numeric',
        ]);

        $status = $request->status;

        $model = AssetSubmission::find($id);
        $model->status = $status;
        $model->save();

        if ($status == 200) {
            $asset = new Asset();
            $asset->code = strtoupper(uniqid());
            $asset->user_id = $model->user_id;
            $asset->name = $model->name;
            $asset->type = $model->type;
            $asset->year = $model->year;
            $asset->price = $model->price;
            $asset->purchase_date = $model->purchase_date;
            $asset->expire_date = $model->expire_date;
            $asset->description = $model->description;
            $asset->status = 100;
            $asset->save();
        }

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Aset $model->name berhasil diverifikasi"
        ]);

        return redirect()->route('submission.index');
    }
}
