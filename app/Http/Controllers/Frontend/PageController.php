<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function landing()
    {
        return view('frontend.landing');
    }

    public function pageAbout()
    {
        return view('frontend.pages.about');
    }

    public function pageService()
    {
        return view('frontend.pages.service');
    }

    public function pageBankAccount()
    {
        return view('frontend.pages.account');
    }
}
