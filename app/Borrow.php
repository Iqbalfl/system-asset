<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Borrow extends Model
{
    protected $appends = ['display_status', 'display_borrow_date', 'display_return_date'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function asset()
    {
        return $this->belongsTo('App\Asset', 'asset_id');
    }

    public function getDisplayBorrowDateAttribute()
    {
        $date = $this->borrow_date;
        return $date != null ? date_dmy($date) : '-';
    }

    public function getDisplayReturnDateAttribute()
    {
        $date = $this->return_date;
        return $date != null ? date_dmy($date) : '-';
    }

    public function getDisplayStatusAttribute()
    {
        $result = '';
        $status = $this->status;

        switch ($status) {
            case '100':
                $result = 'Menunggu Persetujuan';
                break;
            case '200':
                $result = 'Disetujui';
                break;
            case '300':
                $result = 'Dikembalikan';
                break;
            case '10':
                $result = 'Ditolak';
                break;
            
            default:
                $result = 'Tidak Ada';
                break;
        }

        return $result;
    }
}
