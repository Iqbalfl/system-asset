<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetSubmission extends Model
{
    protected $appends = ['display_status', 'display_submission_date'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function getDisplayStatusAttribute()
    {
        $result = '';
        $status = $this->status;

        switch ($status) {
            case '100':
                $result = 'Menunggu Persetujuan';
                break;
            case '200':
                $result = 'Disetujui';
                break;
            case '10':
                $result = 'Ditolak';
                break;
            
            default:
                $result = 'Tidak Ada';
                break;
        }

        return $result;
    }

    public function getDisplaySubmissionDateAttribute()
    {
        $date = $this->submission_date;
        return date_dmy($date);
    }
}
