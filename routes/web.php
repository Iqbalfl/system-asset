<?php

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return redirect()->route('login');
// });

Route::get('/', 'Frontend\PageController@landing')->name('landing');
Route::get('/about', 'Frontend\PageController@pageAbout')->name('page.about');
Route::get('/service', 'Frontend\PageController@pageService')->name('page.service');
Route::get('/register-bank-account', 'Frontend\PageController@pageBankAccount')->name('page.account');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/account/profile', 'ProfileController@show')->name('profile.show');
Route::put('/account/profile', 'ProfileController@update')->name('profile.update');
Route::put('/account/profile/member', 'ProfileController@updateMember')->name('profile.update.member');

Route::group(['prefix' => 'dash', 'middleware' => ['auth', 'role:admin']], function () {
    Route::get('user', 'Backend\UserController@index' )->name('user.index');
    Route::post('user', 'Backend\UserController@store' )->name('user.store');
    Route::get('user/create', 'Backend\UserController@create' )->name('user.create');
    Route::get('user/{id}/edit', 'Backend\UserController@edit' )->name('user.edit');
    Route::put('user/{id}/update', 'Backend\UserController@update' )->name('user.update');
    Route::delete('user/{id}', 'Backend\UserController@destroy' )->name('user.destroy');
    
    Route::resource('report', 'Backend\ReportController');
});

Route::group(['prefix' => 'dash', 'middleware' => ['auth', 'role:admin|user']], function () {
    Route::get('submission/{id}/verification', 'Backend\SubmissionController@verification' )->name('submission.verification');
    Route::patch('submission/{id}/verification', 'Backend\SubmissionController@verificationStore' )->name('submission.verification.store');

    Route::get('borrow/{id}/verification', 'Backend\BorrowController@verification' )->name('borrow.verification');
    Route::patch('borrow/{id}/verification', 'Backend\BorrowController@verificationStore' )->name('borrow.verification.store');
    Route::patch('borrow/return', 'Backend\BorrowController@returnStore' )->name('borrow.return.store');

    Route::resource('borrow', 'Backend\BorrowController');
    Route::resource('submission', 'Backend\SubmissionController');
    Route::resource('asset', 'Backend\AssetController');
});